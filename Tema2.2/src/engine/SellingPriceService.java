package engine;

import compute.Car;
import compute.ISelPriceService;

public class SellingPriceService implements ISelPriceService {

	@Override
	public double computeSellingPrice(Car c) {
		double priceSelling = c.getPricePurchasing() - c.getPricePurchasing() / 7 * (2015 - c.getYear());
		if (priceSelling < 0) {
			priceSelling = (-1) * priceSelling;
		}
		return priceSelling;
	}

}
