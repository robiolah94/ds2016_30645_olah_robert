package engine;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import compute.ISelPriceService;
import compute.ITaxService;

public class ServerStart {
	// private static final Log LOGGER = LogFactory.getLog(ServerStart.class);

	private ServerStart() {
	}

	private static final int PORT = 8889;

	public static void main(String[] args) {

		try {
			String nametax = "ITaxService";
			String nameprice = "ISelPriceService";

			ITaxService tax = new TaxService();
			ISelPriceService selPriceService = new SellingPriceService();

			ITaxService stub1 = (ITaxService) UnicastRemoteObject.exportObject(tax, 0);
			ISelPriceService stub2 = (ISelPriceService) UnicastRemoteObject.exportObject(selPriceService, 0);

			LocateRegistry.createRegistry(1099);
			Registry registry = LocateRegistry.getRegistry();
			registry.rebind(nametax, stub1);
			registry.rebind(nameprice, stub2);

			System.out.println("Server has started");
		} catch (Exception e) {
			System.err.println("ComputeEngine exception:");
			e.printStackTrace();
		}
	}
}
