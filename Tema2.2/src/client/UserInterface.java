package client;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class UserInterface extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	JButton btnCalculate;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserInterface frame = new UserInterface();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UserInterface() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 254);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel label1 = new JLabel("Fabrication Year :");
		label1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		label1.setBounds(21, 25, 146, 32);
		contentPane.add(label1);

		JLabel label2 = new JLabel("Engine Size :");
		label2.setFont(new Font("Tahoma", Font.PLAIN, 17));
		label2.setBounds(21, 76, 103, 32);
		contentPane.add(label2);

		JLabel label3 = new JLabel("Purchasing Price:");
		label3.setFont(new Font("Tahoma", Font.PLAIN, 17));
		label3.setBounds(21, 124, 146, 32);
		contentPane.add(label3);

		textField = new JTextField();
		textField.setBounds(167, 34, 103, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(167, 85, 103, 20);
		contentPane.add(textField_1);

		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(167, 133, 103, 20);
		contentPane.add(textField_2);

		JLabel label4 = new JLabel(" Car Tax:");
		label4.setFont(new Font("Tahoma", Font.PLAIN, 17));
		label4.setBounds(314, 25, 80, 32);
		contentPane.add(label4);

		JLabel lblSellingPrice = new JLabel(" Selling Price:");
		lblSellingPrice.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblSellingPrice.setBounds(314, 76, 103, 32);
		contentPane.add(lblSellingPrice);

		textField_3 = new JTextField();
		textField_3.setBounds(441, 34, 121, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);

		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(441, 85, 121, 20);
		contentPane.add(textField_4);

		btnCalculate = new JButton("Calculate");
		btnCalculate.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnCalculate.setBounds(374, 133, 156, 42);
		contentPane.add(btnCalculate);
	}

	public void addBtnGetActionListener(ActionListener e) {
		btnCalculate.addActionListener(e);
	}

	public String getYear() {
		return textField.getText();
	}

	public String getEngineSize() {
		return textField_1.getText();
	}

	public String getPurchasingPrice() {
		return textField_2.getText();
	}

	public void printTax(Double rez) {
		textField_3.setText(rez.toString());
	}

	public void printSelPrice(Double rez) {
		textField_4.setText(rez.toString());
	}

}
