package client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import compute.Car;
import compute.ISelPriceService;
import compute.ITaxService;

public class UserInterfaceController {

	private UserInterface View;

	public UserInterfaceController() {
		View = new UserInterface();
		View.setVisible(true);

		View.addBtnGetActionListener(new GetActionListener());
	}

	class GetActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			try {
				int fabricationYear = Integer.parseInt(View.getYear());
				int engineSize = Integer.parseInt(View.getEngineSize());
				double purchasingPrice = Double.parseDouble(View.getPurchasingPrice());
				Car car = new Car(fabricationYear, engineSize, purchasingPrice);

				String name1 = "ITaxService";
				String name2 = "ISelPriceService";

				Registry registry = LocateRegistry.getRegistry(1099);

				ITaxService tax = (ITaxService) registry.lookup(name1);
				ISelPriceService sel = (ISelPriceService) registry.lookup(name2);

				Double resultTax = tax.computeTax(car);
				Double resultSelPrice = sel.computeSellingPrice(car);

				View.printTax(resultTax);
				View.printSelPrice(resultSelPrice);

				System.out.println(resultTax);
				System.out.println(resultSelPrice);

			} catch (NumberFormatException ex) {

			} catch (Exception e) {
				System.err.println("Calculate Tax exception:");
				e.printStackTrace();
			}
		}
	}
}
