package compute;

import java.rmi.Remote;
import java.rmi.RemoteException;


public interface ISelPriceService extends Remote{

	
	double computeSellingPrice(Car c) throws RemoteException ;
}
