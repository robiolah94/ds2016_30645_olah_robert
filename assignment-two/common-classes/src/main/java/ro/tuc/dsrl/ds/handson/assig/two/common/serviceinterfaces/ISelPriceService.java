package ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;

public interface ISelPriceService {

	
	double computeSellingPrice(Car c);
}
