package ro.tuc.dsrl.ds.handson.assig.two.server.services;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ISelPriceService;

public class SellingPriceService implements ISelPriceService{

	@Override
	public double computeSellingPrice(Car c) {
		double priceSelling = c.getPricePurchasing()-c.getPricePurchasing()/7*(2015-c.getYear());
        if(priceSelling<0)
        {
        	priceSelling=(-1)*priceSelling;
        }
		return priceSelling;
	}

}
